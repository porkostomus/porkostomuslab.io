{:title "Just Juxt #20: Prime Numbers (4clojure #67)"
 :layout :post
 :klipse true
 :tags  ["Cryogen" "KLIPSE" "juxt" "4clojure" "coding exercises"]}
 
![Prime](/img/prime.png)

>Write a function which returns the first x number of prime numbers.

```klipse-cljs
(ns live.test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]))

(defn primes [n] 
  ((comp (partial apply take) reverse list)
   (remove (comp (partial apply some)
                 (juxt (partial partial (comp zero? rem))
                       (partial range 2)))
           (iterate inc 2)) n))

(deftest primes-test
  (is (= (primes 2) [2 3]))
  (is (= (primes 5) [2 3 5 7 11]))
  (is (= (last (primes 100)) 541)))
  
(run-tests)
```