{:title "100 Days Of Clojure Code - Day 1"
 :layout :post
 :klipse true
 :tags  ["KLIPSE" "coding exercises" "100 Days Of Code"]}
 
![100 Days of Code](/img/100DaysOfCode.jpg)
 
Alright, so here we go! For the first day of the [100 Days Of Code](https://www.100daysofcode.com/) challenge I did the same thing I've been doing every day for the past month, I published a [Just Juxt](https://porkostomus.gitlab.io/posts-output/2018-09-15-Just-Juxt-32/).
 
Let's see what the other participants did!

Taking a look at [jr0cket's log](https://github.com/jr0cket/100-days-of-clojure-code/blob/master/log.md), he already began yesterday by testing and describing his development environment, so I ought to do something like that too.

The text editor I use is [Bob](https://github.com/porkostomus/bob), which I built myself in C. It's nothing fancy, it doesn't even have undo. We like livin' dangerously here, me and Bob, on Planet Porkostomus. I do plan on giving it REPL support as soon as I figure out how. I might use the [Socket REPL](https://clojure.org/reference/repl_and_main#_launching_a_socket_server), since it's just Telnet. Or better yet - pREPL, which I hear uses structured EDN data.

![Bob](/img/bob.png)

I also have [Spacemacs](http://spacemacs.org/), [Vim](https://www.vim.org/) with [fireplace](https://github.com/tpope/vim-fireplace) and [Atom with proto-repl](https://atom.io/packages/proto-repl) which may come in handy for its visualizations. But mostly I just work in the terminal, in my little Bob the text editor, and paste stuff into the REPL. Then when I get something I want to present, I usually put it into a page in self-hosted Clojurescript. That produces a nice environment with constant evaluation that I find very handy.

So let's see what jr0cket is doing today... it's a [webapp-status-monitor](https://github.com/jr0cket/webapp-status-monitor), the start of a very simple status monitor mock application.

And then we have [kazesberger](https://github.com/kazesberger/100-days-of-clojure-code), who is working on a [Minesweeper](https://exercism.io/my/solutions/6b8644558388479fbedcb8db4f751f16) exercise.

I'm a mentor on [exercism](https://exercism.io/) and didn't even realize they had a Minesweeper. I would have looked to it for help back when I was making [my own](https://github.com/porkostomus/tsweep). It was quite a mind-bender, an unexpectedly difficult problem.

Let's check it out:

```klipse-cljs
(ns minesweeper
  (:require [cljs.test :refer-macros [deftest testing is run-tests]]
             [clojure.string :refer [join]]))
             
(defn draw [] ;; <- arglist goes here
  ;; your code goes here
  )
  
(def line-separator \n)

(deftest zero-size-board
  (is (= (draw "") "")))

(deftest empty-board
  (is (= (draw (join line-separator ["   "
                               "   "
                               "   "]))
         (join line-separator ["   "
                         "   "
                         "   "]))))

(deftest surrounded
  (is (= (draw (join line-separator ["***"
                               "* *"
                               "***"]))
         (join line-separator ["***"
                         "*8*"
                         "***"]))))

(deftest board-full-of-mines
  (is (= (draw (join line-separator ["***"
                               "***"
                               "***"]))
         (join line-separator ["***"
                         "***"
                         "***"]))))

(deftest horizontal-line
  (is (= (draw " * * ")
         "1*2*1")))

(deftest vertical-line
  (is (= (draw (join line-separator [" "
                               "*"
                               " "
                               "*"
                               " "]))
         (join line-separator ["1"
                         "*"
                         "2"
                         "*"
                         "1"]))))

(deftest cross
  (is (= (draw (join line-separator ["  *  "
                               "  *  "
                               "*****"
                               "  *  "
                               "  *  "]))
         (join line-separator [" 2*2 "
                         "25*52"
                         "*****"
                         "25*52"
                         " 2*2 "]))))
                         
(run-tests)
```

Now if we take a look over at Exhibit B, jr0cket is cooking up something pretty nice. And he's doing a great job documenting it, too! It's a Ring app with Compojure, Hiccup and Bootstrap. Let's try cloning the [repository](https://github.com/jr0cket/webapp-status-monitor) and firing it up:

![jr0cket](/img/jr0cket.png)

Awesome!

We'll be checking back periodically to see how we're progressing. Excited to see!

![Mr. PB](/img/poopy.jpg)