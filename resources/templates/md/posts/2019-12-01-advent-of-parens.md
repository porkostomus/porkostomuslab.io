{:title "Advent of Parens 2019"
 :layout :post
 :klipse true
 :tags  ["Advent of Parens"]}
 
![Advent of Parens 2019 Day 1](/img/advent.png)

Here we go! So much to get around to, so much to catch up on since my last post which was 4 months ago.
Last season I was just getting into UI work with Reagent.
Then I started making bigger apps with re-frame, and have [something very exciting](https://github.com/porkostomus/mecca) to share.

But first... I have to do a thing for a job interview! It's... 

A twitterbot!

Which is actually really funny, because readers of this blog may remember that it was actually founded on a [very similar project](https://twitter.com/just_juxt)!

This kind of thing was also given a very nice (and thorough) treatment on the [Functional Design in Clojure](https://clojuredesign.club/) podcast.

Thanks a lot! I learned a whole bunch. My potential employer was likely betting on the applicants not listening to those shows.

Happy Advent!

Please check out the blogs of the other participants:

[Arne Brasseur](https://lambdaisland.com/blog/2019-11-25-advent-of-parens)

[Alexander Oloo](https://alexanderoloo.com/)

[John Stevenson](https://practicalli.github.io/blog/)