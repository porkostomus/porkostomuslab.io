{:title "Sum Some Set Subsets"
 :layout :post
 :klipse true
 :tags  ["4clojure" "KLIPSE" "coding exercises" "Clojure"]}
 
 First we will generate the set of all subsets:
 
```klipse-cljs
(defn powerset [s]
  (reduce #(into % (for [subset %] (conj subset %2)))
          #{#{}} s))

(powerset #{-1 1 99} #{-2 2 888} #{-3 3 7777})
```

However, we only want the non-empty ones:

```klipse-cljs
(defn non-empty-subsets [sets]
  (map #(disj (powerset sets) #{}) sets))

(non-empty-subsets #{-1 1 99} #{-2 2 888} #{-3 3 7777})
```

From this we can generate the sum of each:

```klipse-cljs
(defn sum-of-sets [sets]
  (set (map #(apply + %) sets)))

(map sum-of-sets
     (non-empty-subsets #{-1 1 99} #{-2 2 888} #{-3 3 7777}))
```
Now we check for matches:

```klipse-cljs
(apply clojure.set/intersection
  (map sum-of-sets
    (non-empty-subsets #{-1 1 99} #{-2 2 888} #{-3 3 7777})))
```