{:title "The MECCA Platform"
:layout :post
:klipse true
:tags  ["Advent of Parens" "MECCA Music Platform"]}

![MECCA Music Platform](/img/mecca.png)

It all started from a simple question - "How do I make the computer play a note?" Now this project has expanded and multiplied and taken over my entire life, and I've become determined to develop it into a piece of software worthy of being called a *platform*.

[Check out the platform on Github](https://github.com/porkostomus/mecca)

## A creative platform

![2d platformer](/img/platform.png)

I did not happen to have this double meaning in mind at the time when I called it a "platform", but it sure would come in handy.
I also don't know if I had ever fully noticed the similarity between the scrolling music editor and 2D side-scroller game mechanics.
Mario enters stage left and moves to the center of the screen, at which point the rest of the view begins scrolling instead.
This, representing my accidental stumble into game dev, also feeds a little pet theory that this game did that on purpose, as a way to sneakily introduce me to another form of media.
Just like it introduced me to the joys of graphical music composition many years ago.

This is the nature of a creative platform. It is entirely wired to encourage playful exploration. I think we could use some more apps like this.

I mentioned that this project has multiplied, and indeed, it has splintered off into a suite of companion projects that I intend to cover in the coming weeks:

* [mecca-pix](https://github.com/porkostomus/mecca-pix) - Tool to convert pixel art to SVG

* [mecca-roms](https://github.com/porkostomus/mecca-roms) - Inspect image, sound and game data from Nintendo binaries

* [mecca-midi](https://github.com/porkostomus/mecca-midi) - Clojurescript app for exploring MIDI files
