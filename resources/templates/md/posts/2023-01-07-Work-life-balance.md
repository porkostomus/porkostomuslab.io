{:title "Work-life balance"
:layout :post
:tags  ["rants"]}

I’ve been wrestling with this topic for years, and have hesitated to share my thoughts because I fear they might be seen as unrelatable, utter fantasy, or possibly even dangerous.

Admittedly, I haven’t quite figured out work, or life for that matter, largely because my neurodivergent mind actually struggles to distinguish between the two. What is work? What is life? Why must they be perpetually in conflict with each other?

Work is nothing more than engaging in some activity that benefits ourselves and others. It is fundamentally social and cooperative. It is the means by which we realize our potential and sustain our existence. I don’t think anyone would argue with that.

Here’s the unrelatable part: I seem to have a completely foreign concept of fun. For example, I don’t play games or watch movies. I don’t even pursue romantic relationships (sorry, Tinder). Most things that people enjoy I find tedious, and the things I enjoy… are the things “normal” people call work.

I wasn’t always like this. I grew up spending all my time making music, and resisted any form of academic activity. I dropped out of high school (college in UK terms) and was not interested in formal education. This began changing when I was in my twenties when I started studying Jewish philosophy, which led me to the sciences, like physics and mathematics. From that point on, I’ve been kind of making up for lost time - after so many years of just having fun, I wanted to better understand how things work and be able to engage in intelligent matters.

After stopping my Rabbinical studies in Israel, I returned to the states and decided to go to university, and majored in music. I could never really imagine doing anything else for a living. But after a few years, something changed and I felt a desire to use my brain more. I always loved science, so I studied mathematics, psychology, and finally computer science, which represented the final synthesis of all of my experiences. I realized that anything in the world can be expressed in code and written in applications that transform our lives in the most efficient way humans are capable of.

The remarkable thing about this realization is that for the first time in my life, I was immensely passionate about something that was not only a realistic occupation, but something that can be applied to any occupation. For this, I consider myself extremely lucky. Very few people ever discover something that they truly love that could earn them a good living.

So what happened to music? Well… I started writing a software music sequencer so I can write music in code. So I guess that’s my work-life balance. When my head starts to get too fuzzy on a project… I just switch to a different project! But I’m always coding, so I never quite step outside of “work”, or at least “work-adjacent” things. And I’d never want to, because I seem to have fused my entire identity into my productive output. The very concept of work is no longer adversarial.

This might sound like a fantasy, but it’s far from all figured out. Of course, to build a responsible, sustainable life there are going to have to be times that I spend doing things when I’d rather be doing something else. And this requires balance, the same way it applies to everyone. But even though I’ve struggled more than many people have to get here, I am incredibly fortunate to have somehow managed to avoid what nearly everyone begrudgingly accepts as an inevitable fact of life - that we have to spend a significant amount of time doing something that sucks.

I really wish this was a more common story.