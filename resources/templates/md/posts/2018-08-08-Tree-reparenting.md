{:title "Tree reparenting, Part 2"
 :layout :post
 :klipse true
 :tags  ["4clojure" "trees" "data visualization" "coding exercises"]}
 
In a [previous post](https://porkostomus.gitlab.io/posts-output/2018-08-05-4clojure-130/) we introduced the tree-reparenting [exercise](http://www.4clojure.com/problem/130).
 
## Tree visualization

To help understand the way we are representing trees (as lists of lists), we'll use the [Rhizome](https://github.com/ztellman/rhizome) library to visualize the test cases, the original tree (left) and the new tree we want (right):

```
(=  (tree-reparent 'a '(t (e) (a)))
   '(a (t (e))))
```

We will transform the tree on the left into the one on the right by "pulling" the `a` node up to the top:

`'(t (e) (a))` -> `'(a (t (e)))`

!['(t (e) (a))](https://gitlab.com/porkostomus/porkostomus.gitlab.io/raw/master/resources/templates/img/treetwoo.png)
!['(a (t (e)))](https://gitlab.com/porkostomus/porkostomus.gitlab.io/raw/master/resources/templates/img/treetwo.png)

All the same connections are left intact, we have simply repositioned the nodes. Here's a slightly more complex tree:

```
(= (tree-reparent 'd '(a (b (c) (d) (e)) (f (g) (h))))
    '(d (b (c) (e) (a (f (g) (h))))))
```

`'(a (b (c) (d) (e)) (f (g) (h)))` -> `'(d (b (c) (e) (a (f (g) (h)))))`

!['(a (t (e)))](https://gitlab.com/porkostomus/porkostomus.gitlab.io/raw/master/resources/templates/img/tree4a.png)

## Solution

The tree above is defined here as `tree1`.
The `tree-seq` function returns a lazy sequence of the nodes in a tree, via a depth-first walk.

```klipse-cljs
(def tree1
  '(a
     (b (c) (d) (e))
     (f (g) (h))))

(tree-seq next rest tree1)
```

The call to `tree-seq` returns a list of lists, one for each node. 
Now `filter` out only the sublists that contain `d`, our new root:

```klipse-cljs
(filter #(some #{'d} (flatten %))
        (tree-seq next rest tree1))
```
That leaves 3 lists. Construct the new tree by removing the items of the second list from the first, and appending the result to the first list:

```klipse-cljs
(reduce
  (fn [a b] (concat b
                   (list (remove #{b} a))))
      '((a (b (c) (d) (e)) (f (g) (h)))
      (b (c) (d) (e))
      (d)))
```

![Finished tree](https://gitlab.com/porkostomus/porkostomus.gitlab.io/raw/master/resources/templates/img/tree4b.png)

We have now built all the pieces to [complete the `tree-reparent` function.](https://porkostomus.gitlab.io/posts-output/2018-08-05-4clojure-130/)