{:title "About"
 :layout :page
 :page-index 0
 :navbar? true}

## Bobbi Towers

![My profile pic](/img/curls-purple.png)

I write software for music and science education, and make instructional videos.

"The Crazy Clojurian" is a twist on [The Little Lisper](https://mitpress.mit.edu/books/little-lisper-trade-edition), which later became The Little Schemer.

![Little Lisper book](/img/lisper.jpg)

'Tis the return of alliteration. 

A porkostomus is a [suckermouth catfish](https://en.wikipedia.org/wiki/Hypostomus_plecostomus) that licks the bacon-grease off of your walls.

![Primus Pork Soda album cover](/img/pork.jpg)

'Tis part of a healthy aquatic ecosystem.

My chronic lifelong obsession of making computer beep boop boop beep in fact goes back to the early 90s when I was hacking with BASIC on my dad's hand-me-down IBM XT. He ran a home business and I got all his old shit, eventually to become his sysadmin guy and had the great pleasure of stewarding not just one major transition but 2:

1. The shift from mechanical to digital desktop publishing
2. The shift from mail-order to web-based sales.

And then I got distracted by music for 15 years. Became a band leader, teacher and sound engineer, then moved to Israel to study the ancient music of the Middle East. After returning to the states and releasing my [debut album](https://open.spotify.com/album/7BD4LSki0D0hvymTABty3g) in 2013, I went back to school, eventually to gravitate back to my original love, telling computers what to do. Because no one else ever listens to me...

The first piece of software I wrote that has seen some actual use was a rather unexpected(?) hero called the [MECCA Music Platform](https://github.com/porkostomus/mecca), the Music Education, Composition & Creation Application, which is a greatly overpretentiously named Chiptune tracker written in Bash.

Go figure. Beep boop boop beep.
